/// Largest prime factor.
///
/// The prime factors of 13195 are 5, 7, 13 and 29.
/// What is the largest prime factor of the number 600851475143?

extern crate cogwheel;


fn main() {
    let lpf = largest_prime_factor(600851475143u64);
    println!("The largetst prime factor of 600851475143 is: {}",
             (lpf as i32));
}

fn largest_prime_factor(n: u64) -> u64 {
    let factors = cogwheel::prime::trial_div(n);
    *factors.iter().max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::largest_prime_factor;
    #[test]
    fn answer_0003() {
        assert_eq!(largest_prime_factor(13195), 29);
        assert_eq!(largest_prime_factor(600851475143), 6857);
    }
}
