//! Multiples of 3 and 5
//!
//! If we list all the natural numbers below 10 that are multiples of 3
//! or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23. Find
//! the sum of all the multiples of 3 or 5 below 1000.

fn main() {
    let sum = sum_multi(3, 5, 1000);
    println!("The sum of the multiples of 5 and 3 under 1000 is: {}", sum);
}

/// Calculate the sum of multiples.
///
/// Calculates the sum of the multiples of x and y, which are less than
/// z, not inclusive.
/// 
fn sum_multi(x: i32, y: i32, z: i32) -> i32 {
    let mut i = 1;
    let mut sum = 0;
    while i < z {
        if i % x == 0 || i % y == 0 {
            sum = sum + i;
        }
        i = i + 1;
    }
    sum
}

#[cfg(test)]
mod tests {
    use super::sum_multi;
    #[test]
    fn simple() {
        assert_eq!(sum_multi(3, 5, 10), 23);
        assert_eq!(sum_multi(5, 3, 10), 23);
    }
    #[test]
    fn answer_0001() {
        assert_eq!(sum_multi(3, 5, 1000), 233168);
        assert_eq!(sum_multi(5, 3, 1000), 233168);
    }
}
