# Project Euler

The [project Euler](https://projecteuler.net/archives) problems in rust.

## How to use

To build the project and run tests:

    cargo build
    cargo test

The solutions follow the naming convention `0001` -> `0xyz` with `xyz` corresponding the number of the problem. The source for each is inside `src/bin` as per the cargo conventions and to run the source for any:

    cargo run --bin 0xyz
